Package.describe({
	name: 'howcloud:rest-api',
	version: '0.1.0',
	summary: 'Exports a facility to define a user authenticated server side restful api',
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore');
	api.use('ddp');
	api.use('mongo');
	api.use('meteor');
	api.use('jquery');
	
	api.use('howcloud:collection-base');
	api.use('howcloud:users', ['client', 'server'], {weak: true});
	api.use('howcloud:router'); // this is what we use to receive the initial requests to the API

	/* Add files */

	api.add_files('API.js', ['server']);

	/* Export */

	api.export('API', ['server']);

});