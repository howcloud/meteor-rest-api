API = {
	endpoints: {},

	register: function (endpoint, func, opts) {
		this.endpoints[endpoint] = {
			f: func,
		}

		if (opts) _.extend(this.endpoints[endpoint], opts);
	},

	enabled: function () {
		return true; // previously could flag as being disabled using a config variable
	}
}

/** APIUsers Collection **/

var APIUsers = {

	collection: new Meteor.Collection("apiUsers"),

	/** New **/

	new: function () {
		var _pubKey = randString(15);
		var _privKey = randString(25);

		var id = APIUsers.collection.insert({
			publicKey: _pubKey,
			privateKey: _privKey,
			active: true,
		});

		return APIUsers.collection.findOne(id);
	},

	/** Authentication **/

	publicKeyAuth: function (_pubKey) {
		var user = APIUsers.collection.findOne({publicKey: _pubKey});
		
		if (user) user.PUBLICAUTH = true;

		return user;
	},
	privateKeyAuth: function (_privKey) {
		var user = APIUsers.collection.findOne({privateKey: _privKey});
		
		if (user) user.PRIVATEAUTH = true;

		return user;
	},

	/** Logging **/

	log: function (_apiUser, endpoint, result) {
		// TODO: More comprehensive logging

		APIUsers.collection.update(_apiUser._id, {
			$set: {
				lastCallDate: new Date()
			}
		});
	},

}

/** Server Side Route [processes API requests] **/

Router.route({
	path: '/_api/:endpoint(.*)?',
	action: function (params, req, res, next) {
		_.extend(params, req.body); // allow post to override GET params

		var result;
		try {

			/** Process params **/
			// If they are valid JSON - parse them and send that value to the API

			var gotParams = {};
			_.each(params, function (value, key) {
				try {
					gotParams[key] = JSON.parse(decodeURIComponent(value).replace(/\+/g, ' '));
				} catch (_err) {
					gotParams[key] = value;
				}
			});

			params = gotParams;
			
			/** Auth **/

			var apiUser; // TODO: turn into an object prototype
			var pubKey = params._publicKey;
			var privKey = params._privateKey;

			if (!pubKey && !privKey) throw "Access denied";

			if (privKey) { // private key gets first preference
				apiUser = APIUsers.privateKeyAuth(privKey);
				if (!apiUser) throw "Access denied [private key invalid]";
			}

			if (pubKey) {
				var pubApiUser = APIUsers.publicKeyAuth(pubKey);
				if (!pubApiUser) throw "Access denied [public key invalid]";
				if (apiUser && pubApiUser._id != apiUser._id) throw "Access denied [public private key mismatch]";
				if (!apiUser) apiUser = pubApiUser;
			}

			apiUser.ownershipIdentifier = function () {
				if (!apiUser.user_id) return null;

				return {
					type: 'user',
					user_id: apiUser.user_id
				}
			}

			/** Find Endpoint **/

			var endpoint = params.endpoint;
			if (endpoint) endpoint = API.endpoints[endpoint];

			/** Eval Endpoint **/

			if (endpoint) {
				if (endpoint.requireAuthPrivate && !apiUser.PRIVATEAUTH) throw "Access denied [private authentication required for call]";
				if (endpoint.requireRoot && apiUser.user_id) throw "Access denied [root authentication required for call]";
				if (endpoint.requireUser && !apiUser.user_id) throw "Access denied [user authentication required for call]";
				if (endpoint.requireIdpService && !apiUser.idpService) throw "Access denied [idp service required for call]";

				result = endpoint.f(params, apiUser); // we make apiUser the second parameter as it may not be required and for backwards compat with all the API endpoints we have already created which don't implement the user interface
			} else {
				throw "Invalid API endpoint: " + params.endpoint;
			}

			/** Log **/

			APIUsers.log(apiUser, endpoint, result);

		} catch (_err) {
			console.log("API Error");

			console.log(_err);
			if (_err.stack) console.log(_err.stack);

			result = {error: _err}
		}

		res.writeHead(200, {
			'Content-Type': 'application/json',

			'Access-Control-Allow-Credentials': 'true',
			'Access-Control-Allow-Origin': '*',
		});
		
		res.end(JSON.stringify(result));

		return true; // => we dealt with this, no need to hand it off to standard processing methods					
	}
});

/** Setup default APIUsers if none exist **/

Meteor.startup(function () {
	if (API.enabled() && !APIUsers.collection.find().fetch().length) APIUsers.new();
});